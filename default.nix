with import <nixpkgs> {}; {
	ProtoLimC = stdenv.mkDerivation {
		name = "ProtoLimC";
		hardeningDisable = [ "all" ];
		buildInputs = [
			cmake
			llvm_10
			lld_10
			clang_10
		];
	};
}
